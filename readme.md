# Python Intro 2

Learn about how Python handles numbers, string, and lists of data. Also learn about importing Python modules so you can use them in your code.

See the [homework description](markdown/homework.md).

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions and methods\n",
    "\n",
    "Yeah, I know the notebook is called \"strings\", but we'll get to that in a minute...\n",
    "\n",
    "So far you've mostly seen Python used as a glorified printer and calculator, but that's not really too useful. You'll use *functions* and *methods* to do more complicated operations. You've already used functions to do things like convert floats to integers and to round numbers, but you haven't seen examples of methods yet. You'll see both in this notebook, so this seems like a good place to explain the difference between the two.\n",
    "\n",
    "A function usually requires you to provide some *parameters* for it to work with. For example, the `round()` function requires you to provide a number to be rounded."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "round(3.14159)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can also accept more than one parameter (some take lots!). For example, you can provide a second parameter to the `round()` function to round a number to a certain number of decimal places. This next bit of code passes a 2 as the second parameter, which means round to two decimal places."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "round(3.14159, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions can also use variables as parameters. This example creates a variable called `pi` and then passes that variable to the `round()` function and rounds it to four decimal places:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pi = 3.14159\n",
    "round(pi, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Methods* are a lot like functions, but they're attached to specific objects. Say you had a variable called `car` and you wanted to drive it. There might be a function called `drive()` that accepted a car as a parameter, like this:\n",
    "\n",
    "```py\n",
    "drive(car)\n",
    "```\n",
    "\n",
    "Or another way of doing things is to have functionality like driving be part of the car itself. In that case you'd call the car's `drive()` method (notice the dot between the `car` object and its `drive()` method):\n",
    "\n",
    "```py\n",
    "car.drive()\n",
    "```\n",
    "\n",
    "Don't worry about the terminology (just don't tell anyone in the computer science department that I said that!) and just be aware that they're two ways to do essentially the same thing. If you're interested, I'd be happy to explain the reasons behind them and pros and cons of each method.\n",
    "\n",
    "You haven't seen a method in action yet, but you'll see some that live on string objects here in a minute."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Strings\n",
    "\n",
    "Strings are used to store text, such as names, addresses, or even whole paragraphs (or more!). Strings are created using either single or double quotes. It doesn't matter which kind of quotes you use, but they do need to match."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('I use single quotes')\n",
    "print(\"I use double quotes\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you don't use `print()` then the quotes around the string are also printed. This lets you know that it's a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'Hello world!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If a number has quotes, then you know it's stored as a string instead of a number (so you can't use it in mathematical calculations without converting it to a number first). For example, trying to add two numbers with quotes around them won't work as you expect:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create two variables with numeric STRINGS in them\n",
    "x = '2'\n",
    "y = '3'\n",
    "\n",
    "# Try to add the two numbers\n",
    "x + y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that you can convert a numeric string to a number, though."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert the numeric strings to integers as they're added together\n",
    "int(x) + int(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to use `x` and `y` as numbers more than once, it makes sense to convert them to numbers once and then you can use them without worrying later on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert the strings to integer and overwrite the original variables\n",
    "x = int(x)\n",
    "y = int(y)\n",
    "\n",
    "# Now they can be used as numbers without converting them again\n",
    "print(x + y)\n",
    "print(y - x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also convert a number to a string. Notice that this has quotes around it when it prints, so you know that it's a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "str(100)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use triple quotes to create a string that has multiple lines in it. In this case you also used `print()` so that the newlines would show correctly. Try changing it so it doesn't use `print` and see what happens (and see the section on escape characters, below, for an explanation)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a multiline string\n",
    "haiku = \"\"\"Haikus are easy.\n",
    "But sometimes they don't make sense.\n",
    "Refrigerator.\"\"\"\n",
    "\n",
    "print(haiku)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to include a single quote in your string, then surround the entire string with double quotes. You can include double quotes in the string by surrounding the string with single quotes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Uses double quotes so an apostrophe can be in the string\n",
    "print(\"Don't panic!\")\n",
    "\n",
    "# Uses single quotes so that double quotes can be in the string\n",
    "print('Kirk yelled, \"Beam me up, Scotty!\"') "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What would happen if you tried to surround \"Don't panic!\" with single quotes?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Don't panic!')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the quote between the `n` and `t` ends the string and Python doesn't know what it's supposed to do with everything after that, so it says that the syntax is invalid. Notice that the arrow points to the `t` after the quote, because that's where it gets confused."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "## Problem 1\n",
    "\n",
    "(3 parts)\n",
    "\n",
    "Print out each of the requested strings. Use `print()` so that the surrounding quotes aren't printed.\n",
    "\n",
    "**1A.** May the Force be with you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**1B.** How inappropriate to call this planet \"Earth\", when it is clearly \"Ocean\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**1C.** You're a wizard, Harry!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get the length of a string\n",
    "\n",
    "The Python `len()` function gets the length of a lot of things, strings included:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len('Hello world')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 2\n",
    "\n",
    "Add a second line to this code that counts how many characters are in the DNA sequence stored in the `dna` variable. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "dna = 'ttcacctatgaatggactgtccccaaagaagtaggacccactaatgcagatcctgtgtgtctagctaagatgtattattctgctgtggatcccactaaagatatattcactgggcttattgggccaatgaaaatatgcaagaaaggaagtttacatgcaaatgggagacagaaagatgtagacaaggaattctatttgtttcctacagtatttgatgagaatgagagtttactcctggaagataatattagaatgtttacaactgcacctgatcaggtggataaggaagatgaagactttcaggaatctaataaaatgcactccatgaatggattcatgtatgggaatcagccgggtctcactatgtgcaaaggagattcggtcgtgtggtacttattcagcgccggaaatgaggccgatgtacatggaatatacttttcaggaaacacatatctgtggagaggagaacggagagacacagcaaacctcttccctcaaacaagtcttacgctccacatgtggcctgacacagaggggacttttaatgttgaatgccttacaactgatcattacacaggcggcatgaagcaaaaatatactgtgaaccaatgcaggcggcagtctgaggattccaccttctacctgggagagaggacatactatatcgcagcagtggaggtggaatgggattattccccacaaagggagtgggattaggagctgcatcatttacaagagcagaatgtttcaaatgcatttttagataagggagagttttacataggctcaaagtacaagaaagttgtgtatcggcagtatactgatagcacattccgtgttccagtggagagaaaagctgaagaagaacatctgggaattctaggtccacaacttcatgcagatgttggagacaaagtcaaaattatctttaaaaacatggccacaaggccctactcaatacatgcccatggggtacaaacagagagttctacagttactccaacattaccaggtaaactctcacttacgtatggaaaatcccagaaagatctggagctggaacagaggattctgcttgtattccatgggcttattattcaactgtggatcaagttaaggacctctacagtggattaattggccccctgattgtttgtcgaagaccttacttgaaagtattcaatcccagaaggaagctggaatttgcccttctgtttctagtttttgatgagaatgaatcttggtacttagatgacaacatcaaaacatactctgatcaccccgagaaagtaaacaaagatgatgaggaattcatagaaagcaataaaatgcatgctattaatggaagaatgtttggaaacct'\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Concatenating strings\n",
    "\n",
    "You'll find that you need to combine strings together fairly often. One way to do this is with the + operator.\n",
    "\n",
    "*I'm not going to use `print()` most of the time here, just because I don't want to type it. Because of that, the surrounding quotes will be printed.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "first_name = 'Micky'\n",
    "last_name = 'Mouse'\n",
    "\n",
    "\"Hi, I'm\" + first_name + last_name + '!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You might've figured out from those results that you need to explicitly add spaces between words you're concatenating, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"Hi, I'm \" + first_name + ' ' + last_name + '!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll get an error if you try to concatenate a string and a number together:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amazon = 4345\n",
    "\n",
    "'The Amazon River is approximately ' + amazon + ' miles long.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the error message tells you exactly what the problem is (`TypeError: cannot concatenate 'str' and 'int' objects`) and has an arrow pointing to the problem line.\n",
    "\n",
    "If you want to combine strings and numbers using the + operator, you need to convert the number to a string first. You can do that using the `str()` function that you saw back in the first section on strings. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 3\n",
    "\n",
    "Modify the following line of code so that it works correctly using the + operator. \n",
    "\n",
    "> Edit the existing line of code and do not add any more lines of code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "'The Amazon River is approximately ' + amazon + ' miles long.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## F-strings\n",
    "\n",
    "Using the + operator is great for simple concatenation, but it's a huge pain once things get complicated (and it's also hard to read). Python 3.6 and later has a great new feature called *f-strings*. These let you insert variables into strings without using `+`. To do this, put the variable name inside the string and surround it by curly braces. You also **must** prefix the string with `f` *before* the beginning quote."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f\"Hi, I'm {first_name} {last_name}!\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens if you forget the `f`?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"Hi, I'm {first_name} {last_name}!\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You don't need to worry about variable types if you use f-strings. Remember how you had to convert `amazon` to a string for problem 2? Check this out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "amazon = 4345\n",
    "\n",
    "f'The Amazon River is approximately {amazon} miles long.'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can even put calculations inside the curly braces, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 2\n",
    "\n",
    "f'{x} plus {x} is {x+x}'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 4\n",
    "\n",
    "Use these variables for the problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "width = 200\n",
    "length = 1500"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "Now use an f-string and the `width` and `length` variables to print out the following exactly as shown here (well, without the empty space at the beginning, because that's just a display issue):\n",
    "\n",
    "```\n",
    "200 * 1500 = 300000\n",
    "```\n",
    "\n",
    "> Use only one line of code, and do not include any numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `format()`\n",
    "\n",
    "Here's your first string method!\n",
    "\n",
    "I love f-strings, but there is an older technique called the `format()` method. You can use this instead of f-strings and still not need to worry about data types.\n",
    "\n",
    "To use `format()`, you create a template string that has placeholders in the locations that you want to insert data. Then you call the `format()` method that automatically lives on the template string and pass it the values that will replace the placeholders. \n",
    "\n",
    "Here's an example, with an explanation afterwards:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template = \"Hi, I'm {0} {1}!\"\n",
    "\n",
    "template.format(first_name, last_name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The curly braces with the numbers inside of them are the placeholders in the template, and the numbers correspond to the parameters passed to `format()`. Whatever placeholder has a 0 inside of it gets the first value passed to `format()`, which is `first_name` in this case. That variable was set to 'Micky' earlier in the notebook.\n",
    "\n",
    "Similarly, the placeholders with a 1 get the second value passed to `format()`, and so on. You can reuse placeholders if you want a value to be included more than once, and they don't need to be in numeric order inside the template string, either. This example shows both of those things, and also passes strings to `format()` instead of using variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Bond is used everywhere there is {1}\n",
    "template = \"The name is {1}, {0} {1}.\"\n",
    "\n",
    "template.format('James', 'Bond')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You won't usually see the template string put into its own variable like that. I only did it because I thought breaking it up might make it easier for you to see what's going on. Here's the same example with `format()` applied to the template string itself rather than putting it in a variable first:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"The name is {1}, {0} {1}.\".format('James', 'Bond')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like f-strings, you don't have to worry about data types. Here's the Amazon example, where `amazon` is a number and doesn't need to be converted to a string for this to work."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'The Amazon River is approximately {0} miles long.'.format(amazon)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just like with f-strings, you can use calculations instead of variables (remember that `x` was set to 2 earlier in the notebook):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'{0} plus {0} is {1}'.format(x, x+x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The numbers inside the placeholders aren't actually necessary if you're going to provide all of the values to replace them with, in order. For example, if I wanted to duplicate the previous example, but without numbers inside the curly braces, I'd have to provide `x` twice, once for each time it was needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'{} plus {} is {}'.format(x, x, x+x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Escape characters\n",
    "\n",
    "Sometimes in programming you need to change the way a character works, or add a special character to a string. To do this you use *escape characters*. Remember the \"Don't panic!\" example that crashed and burned?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'Don't panic!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you really wanted to use single quotes to surround your string, you could use an escape character to keep the apostrophe from messing things up. To do this, insert a backslash (\\\\) right before the apostrophe. This tells Python to include that character in the string instead of using it to end the string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "'Don\\'t panic!'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other escape characters include:\n",
    "\n",
    "* `\\\"` - Double quotation mark\n",
    "* `\\t` - Tab\n",
    "* `\\n` - New line\n",
    "* `\\\\` - Backslash\n",
    "\n",
    "For example, this uses `\\n` to insert two new lines and `\\t` to insert a tab (I used `print()` here so that the newlines would be displayed correctly):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('-Important bullet point\\n-Another important point\\n\\t-Something related')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember the haiku from earlier? We used `print()` to look at it before, so it printed out nicely formatted. If we look at it without using `print()`, then the newline escape characters are shown instead of actually creating new lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "haiku"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filenames and paths\n",
    "\n",
    "Because Windows uses backslashes in file paths, parts of your filenames are likely to be interpreted as escape characters. For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('C:\\temp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `\\t` got replaced by a tab, and Python won't be able to open this file if you ask it to. The easiest way to solve this problem is to prefix the string with an `r`. This turns it into a *raw* string that isn't processed for escape characters. This is the easiest method if you're copy/pasting paths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(r'C:\\temp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also use the `\\\\` escape character to insert a single backslash into the string. You don't need the `r` in this case. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('C:\\\\temp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also pretend to be on a Mac or Linux and use forward slashes instead, and Windows will understand what you mean. I do this if I'm typing the path manually. In this case you don't need the `r` because a forward slash doesn't mean anything special and won't create a tab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('C:/temp')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## String methods\n",
    "\n",
    "Remember the `format()` method you used to format strings? Well, strings have lots of other methods, too. For example, `upper()` converts a string to uppercase."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "name = 'Bugs bunny'\n",
    "name.upper()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here are some examples of string methods. See the code for examples of their use. \n",
    "\n",
    "- Change capitalization: `upper`, `lower`, `capitalize`\n",
    "- Remove excess whitespace: `strip`\n",
    "- Find the location of substrings: `find`\n",
    "- Split a string into pieces: `split`\n",
    "- Count the number of occurrences of particular characters: `count`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define some variables to use in the examples\n",
    "genus = 'Dipodomys'\n",
    "species = '    spectabilis'\n",
    "dna_seq = 'atgcagatcctgtgtgtctagctaag'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert genus to lower case\n",
    "genus.lower()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert species to upper case\n",
    "species.upper()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Strip the spaces from the beginning of species\n",
    "species.strip()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Find the position of 'tcct' in the dna_seq string\n",
    "dna_seq.find('tcct')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Count the number of a's in the dna_seq string\n",
    "dna_seq.count('a')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `split` method divides a string up by whitespace and returns the results as a list (you'll learn about lists in a later notebook): "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "latin_binomial = 'Dipodomys ordii'\n",
    "latin_binomial.split()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Do you remember that you can assign more than one variable at a time? You can take advantage of that fact and assign the `split` results to multiple variables, like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "genus, species = latin_binomial.split()\n",
    "print(f\"The genus in latin_binomial is: {genus}\")\n",
    "print(f\"The species in latin_binomial is: {species}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reading the documentation\n",
    "\n",
    "Now you get to familiarize yourself with the online Python documentation at https://docs.python.org/3.7/ (you should probably bookmark this link!). Being able to effectively use documentation is extremely important, because nobody can remember everything they need to know in order to write scripts. I always have about a million browser tabs open at a time, and most of them are documentation about various things.\n",
    "\n",
    "The online help for a function or method gives you a description of what it does and shows you the syntax required. Square brackets around a parameter name mean it's optional. For example, the documentation for the [find()](https://docs.python.org/3.7/library/stdtypes.html#str.find) method on strings looks something like this:\n",
    "\n",
    "![find](images/find.png)\n",
    "\n",
    "This means that the `sub` parameter is required, but the square brackets indicate that the indexes to start and end the search are optional. In the following example, \"is\" can't be found the second time because you don't start looking until later in the string:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "msg = 'Python is so useful!'\n",
    "print(msg.find('is'))      # Start looking for 'is' at the beginning of msg\n",
    "print(msg.find('is', 10))  # Start looking for 'is' at position 10 of msg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> If you're counting, you might have thought that 'is' was at position 8 in 'Python is so useful!', but the first character is position 0. So start counting at 0 instead of 1 and you'll find that 'is' starts at position 7."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "### Problem 5\n",
    "\n",
    "(5 parts)\n",
    "\n",
    "Use string methods for the next few problems. See https://docs.python.org/3.6/library/stdtypes.html#string-methods for a list of available methods.\n",
    "\n",
    "**Example:** Are all characters in the `example` variable alphanumeric?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example = 'Hello world!' # This is the line provided in the question\n",
    "\n",
    "example.isalnum()        # You add one line for the answer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Just FYI, the example printed `False` because of the exclamation point at the end of 'Hello world!'.*\n",
    "\n",
    "Now go ahead and use the online documentation to figure out how to answer the next few questions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**5A.** Print the value of the `county` variable with just the first letter capitalized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "county = 'CACHE'\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**5B.** Print the value of the `msg` variable without the leading whitespace."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": [
    "msg = \"    Thank goodness it's Friday\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**5C.** How many times does 'a' occur in the DNA string? (Use the `dna` variable from the last cell. It's still in memory, so no need to paste it in here.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**5D.** How many times does the sequence `'gagg'` occur in the `dna` variable?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "source": [
    "**5E.** What is the starting position of the first occurrence of `'atta'` in the `dna` variable?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": [
     "hw"
    ]
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "426.667px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

# Python Intro 2 homework

*Due Monday, February 1, at midnight*

This week you have a few Jupyter Notebooks to work through, just like last week. You'll have some of these every week, and most of them will have small problems in them. Even if they don't have problems, you need to work through them and turn in your completed notebook, because I'll be looking to see that you ran the code in them. You'll want to work through the notebooks and do the problems before you attempt the rest of the homework for that week. The notebooks are worth 15% of your grade. Think of these as the readings from another type of class.

The more self-directed part of your homework is worth 70% of your grade and not only uses the information from the notebooks, but will also require you to use online documentation. There's no way you can remember everything (I certainly don't!) so learning how to use the documentation is essential.

You'll have a project later on that makes up the remaining 15% of your grade.

## Notebook comprehension questions

*17 points*

Work through the following notebooks, do the problems in them, and turn them all in (even the ones without homework problems). Not only will I be looking at your answers to the problems, but I'll be looking to see that you ran all of the code (which hopefully means you read and understood the explanations!). Remember that you can change code to see what happens, which is an extremely good idea if you're not quite sure you understand what's going on.

Just like the notebooks from last week, you need to open Jupyter Notebook from the Start menu and open the notebooks from in there (double-clicking on the files doesn't work).

Each problem is worth one point (except they're numbered weird-- some of the problems have multiple parts, but each part is worth a point). You also get a point for running all of the code in the notebook. **Don't ignore notebooks that are worth fewer points, because you absolutely need to know all of this stuff in order to succeed in the class.**

Work through the notebooks in this order (don't forget to save them!):

- 1-numbers.ipynb (1 point)
- 2-strings.ipynb (12 points) - this one is long!
- 3-lists.ipynb (3 points)
- 4-modules.ipynb (1 point)

When you're ready to turn them in, you can use GitHub Desktop, just like last week (see the `5-finish.html` file from last week if you don't remember how to do it). You can turn each one in as you finish it, or turn them in all at once. It doesn't matter.

## Script problems

*5 points each, for a total of 15 points*

*Remember that although these scripts may be worth less points than the notebooks, they're weighted much more heavily for your final grade.*

Usually I'll give you problem descriptions and you'll need to create a notebook for each problem, but this week I created the notebooks for you. The problem description is at the top of the notebook, and then I added code cells with comments to walk you through the steps. You can work on these in any order:

- problem1.ipynb
- problem2.ipynb
- problem3.ipynb

Turn them in using GitHub Desktop, just like the other notebooks. You can turn each one in as you finish it, or turn them in all at once. It doesn't matter. Remember that you can stop GitLab from nagging you about the due date if you add `Closes #1` to your last commit for the assignment.

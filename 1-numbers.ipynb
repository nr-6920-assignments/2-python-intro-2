{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numbers\n",
    "\n",
    "Anything you can do with a calculator, you can do in Python. Here are some basic calculations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "2 + 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 - 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "2 * 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "10 / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You'll work with two types of numbers on a regular basis: integers and floating point. We all know that integers are whole numbers and floating point is anything else (excluding imaginary numbers, but let's not go there!). \n",
    "\n",
    "You might be surprised to learn that 4.0 isn't considered an integer in Python. This is because anything with a decimal point is considered floating point. So 4 is an integer, but 4.0 is floating point. You can use the `type` function to find out what kind of data Python thinks something is if you really need to know."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(4.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Don't include commas in numbers, because they mean a totally different thing in Python. Depending on the context, you might get an error, or you might get unexpected results like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "1,100"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Converting numbers between types\n",
    "\n",
    "You can convert integers to floats and vice versa."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert float to integer\n",
    "int(5.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Convert integer to float\n",
    "float(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When converting a float to integer, the value is truncated, not rounded. For example, you'd expect 3.9 to be rounded to 4.0, but that doesn't happen if you just convert 3.9 to an integer using `int()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int(3.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the `round` function if you need rounding behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "round(3.9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also convert strings (denoted by the quotes) to numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int('8')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "float('8.2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Python doesn't want you to convert a string that looks like a float to an integer, though *(see the section on errors below for an explanation of this error message)*:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int('8.2')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But if you really need it as an integer, you can always convert it to float and then to an int:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a string variable that looks like a number\n",
    "x_string = '8.2'\n",
    "\n",
    "# Convert the string to a float\n",
    "x = float(x_string)\n",
    "\n",
    "# Convert the float to an integer\n",
    "int(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That converted the string `'8.2'` into the floating point number `8.2` and stored it in the `x` variable. Then it converted the floating point `8.2` stored in `x` into an integer with `int()`.\n",
    "\n",
    "A shorter way to do the same thing is to get rid of the intermediate `x` variable and pass the result of `float(x_string)` directly into `int()`. But if using the variable makes more sense to you, then by all means do it that way."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "int(float(x_string))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# More on errors\n",
    "\n",
    "Let's go back to that error message now. Here's the error message again:\n",
    "\n",
    "```\n",
    "ValueError: invalid literal for int() with base 10: '8.2'\n",
    "```\n",
    "\n",
    "A *literal* is something that can't change, like a string. This message is saying that the string '8.2' that was passed to the `int()` function can't be converted to a base 10 integer. (In case you've forgotten from your math classes, base 10 numbers are the kind we usually use, but other bases are used for other purposes. Those 1's and 0's that computer code gets converted into are base 2 numbers.)\n",
    "\n",
    "```\n",
    "----> 1 int('8.2')\n",
    "```\n",
    "\n",
    "There's an arrow pointing to the offending line of code, so you know there's a string literal on this line that can't be converted to a base 10 integer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Make sure you save your notebook!**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
